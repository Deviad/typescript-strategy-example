import 'reflect-metadata';
import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, Index, OneToMany} from 'typeorm';
import { injectable } from 'inversify';
import {IGenericEntity} from './IGenericEntity';
import {Token} from './';

export interface IUser extends IGenericEntity<IUser> {
  email: string;
  name: string;
  lastName: string;
  age: number;
  password: string;
  id?: number;
  role: number;
  token?: Token;
  tokens?: Token[];
  create?: (user: User) => IUser;
}

@injectable()
@Entity('users')
export class User extends BaseEntity implements IUser {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({nullable: false})
  @Index({ unique: true })
  public email: string;

  @Column({nullable: false})
  public password: string;

  @Column({nullable: false})
  public name: string;

  @Column({nullable: false})
  public lastName: string;

  @Column({type: 'int', nullable: false})
  public age: number;

  @Column({type: 'int', nullable: false})
  public role: number;

  @Column({ type: 'json', nullable: true })
  @OneToMany(
    type => Token,
    token => token.user, {
      cascade: ['update'],
      eager: true
    })
  public tokens: Token[];

  constructor() {
    super();
  }

  public create(user: IUser) {
    const _user = new User();
    _user.email = user.email;
    _user.name = user.name;
    _user.lastName = user.lastName;
    _user.age = user.age;
    _user.password = user.password;
    _user.role = user.role;
    return _user;
  }
}





