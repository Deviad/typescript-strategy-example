import {injectable} from 'inversify';
import { TypeORMCLient } from '../utils/sqldb/client';
import { User } from '../model';

@injectable()
class UserService extends TypeORMCLient<User> {
     constructor() {
         super(User);
     }
}

export default UserService;
