/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 14/03/2018 02:05:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `timestamp` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1520981788178, 'CreateTables1520981788178');
COMMIT;

-- ----------------------------
-- Table structure for tokens
-- ----------------------------
DROP TABLE IF EXISTS `tokens`;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_6a8ca5961656d13c16c04079dd` (`token`),
  KEY `FK_20a1c32e04c1bde78d3f277ba6e` (`user`),
  CONSTRAINT `FK_20a1c32e04c1bde78d3f277ba6e` FOREIGN KEY (`user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tokens
-- ----------------------------
BEGIN;
INSERT INTO `tokens` VALUES (1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImRhdmlkZTFAZ21haWwuY29tIiwicm9sZSI6MSwiaWF0IjoxNTIwOTg1ODc5LCJleHAiOjE1NTIwODk4Nzl9.dE6EvXMzZQ3rWpwMzhATl8AdrZP7tIkXpqol-OTg4_Y', 1);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `tokens` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_97672ac88f789774dd47f7c8be` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'davide1@gmail.com', '$2a$10$3WH3A8DiidvMtVeLSjU8KeW1KMIWOSsxAHpJ8z8VB7AHgGxXdEKWG', 'dsadsad', 'whatasasdadsasdasever', 33, 1, '[{\"id\": 1, \"age\": 33, \"name\": \"dsadsad\", \"role\": 1, \"email\": \"davide1@gmail.com\", \"tokens\": [{\"id\": 1, \"user\": 1, \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImRhdmlkZTFAZ21haWwuY29tIiwicm9sZSI6MSwiaWF0IjoxNTIwOTg1ODc5LCJleHAiOjE1NTIwODk4Nzl9.dE6EvXMzZQ3rWpwMzhATl8AdrZP7tIkXpqol-OTg4_Y\"}], \"lastName\": \"whatasasdadsasdasever\", \"password\": \"$2a$10$3WH3A8DiidvMtVeLSjU8KeW1KMIWOSsxAHpJ8z8VB7AHgGxXdEKWG\"}]');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
