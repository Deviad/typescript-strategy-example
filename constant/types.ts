const TYPES = {
  AuthMiddleware: Symbol.for('AuthMiddleware'),
  TokenService: Symbol.for('TokenService'),
  TypeORMCLient: Symbol.for('TypeORMCLient'),
  UserService: Symbol.for('UserService')
};

export default TYPES;
