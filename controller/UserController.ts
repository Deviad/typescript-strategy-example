import * as express from 'express';
import {
  interfaces,
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
  request,
  queryParam,
  response,
  requestParam, BaseHttpController
} from 'inversify-express-utils';
import {inject, injectable} from 'inversify';
import jwtservice = require('jsonwebtoken');
import jwt = require('express-jwt');
import {signup, login, Joi} from '../validators/';
import {Token, User} from '../model';
import TYPES from '../constant/types';
import {UserService, PasswordService, TokenService} from '../service';
import config from '../config';
import {ExpiredToken, NotEnoughPrivileges} from './errors';
import {createQueryBuilder} from 'typeorm';
import { map } from 'lodash/fp';

// import CircularJSON = require('circular-json');


/* JSON responses are compliant with Jsend specifications
   https://labs.omniti.com/labs/jsend
*/



@controller('/user', jwt({secret: config.server.secret}).unless({path: ['/user/login', '/user/signup']}))

export class UserController extends BaseHttpController {
  private secret: string;

  constructor(@inject(TYPES.UserService) private userService: UserService,
              @inject(TYPES.TokenService) private tokenService: TokenService,
              @inject(PasswordService) private passwordService: PasswordService) {
    super();
    this.secret = config.server.secret;
  }

  @httpGet('/')
  public async getUsers(@request() req: any, @response() res: express.Response): Promise<void> {
    try {
      res.status(200).json({status: 'success', data: await this.userService.find() as void | User[]});
    } catch (err) {
      res.status(400).json({status: 'error', message: err.message});
    }
  }
}
