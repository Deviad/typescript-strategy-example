export class TypeORMError extends Error {
  constructor(public code: 400, public message: string, ) {
    super(message);
  }
}
export class ACLError extends Error {
  constructor(public code: 400, public message: string, ) {
    super(message);
  }
}
// export class NoEntityFoundWithId extends TypeORMError {
//   constructor(id) {
//     super(400, 'No Entity Found with id ' + id);
//   }
// }


export class NotEnoughPrivileges extends ACLError {
  constructor() {
    super(400, 'Not Enough Privileges');
  }
}

export class ExpiredToken extends ACLError {
  constructor() {
    super(400, 'Your token has expired');
  }
}
